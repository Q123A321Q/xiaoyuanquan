import axios from 'axios';
import { API_URL } from './config';

let request = axios.create({
    baseURL: API_URL?"":"",
    timeout: 5000,
});

export function GetRequest(url, data = {}, method = 'GET') {
    return request.get(url)
}
export function PostRequest(url, data = {}, method = 'post') {
    return request.post(url, data)
}
export function getUser(){
    return GetRequest('/api/user');
}

